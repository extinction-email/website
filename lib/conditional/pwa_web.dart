import 'dart:async';
import 'dart:html';

BeforeInstallPromptEvent deferredPrompt;
DisplayMode displayMode;

class ProgressiveWebAppManager {
  static Future initialize({showImmediately = false}) {
    Completer prompt = Completer();
    window.addEventListener('beforeinstallprompt', (event) {
      deferredPrompt = event;
      if (showImmediately) deferredPrompt.prompt();
      prompt.complete();
    });
    window.addEventListener('DOMContentLoaded', (event) {
      displayMode = DisplayMode.BROWSER_TAB;
      if (window.matchMedia('(display-mode: standalone)').matches) {
        displayMode = DisplayMode.STANDALONE;
      }
    });
    return prompt.future;
  }

  static Future<bool> showInstallPromotion() async {
    if (!ProgressiveWebAppManager.isInstallable) return false;
    deferredPrompt.prompt();
    return (await deferredPrompt.userChoice)['outcome'] == 'accepted';
  }

  static bool get isStandalone => displayMode == DisplayMode.STANDALONE;

  static bool get isInstallable => deferredPrompt != null;
}

enum DisplayMode { STANDALONE, BROWSER_TAB }
