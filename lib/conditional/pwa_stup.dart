import 'dart:async';

DisplayMode displayMode;

class ProgressiveWebAppManager {
  static Future initialize({showImmediately = false}) {
    return Completer().future;
  }

  static Future<bool> showInstallPromotion() async {
    return false;
  }

  static bool get isLaunchedAsPwa => false;

  static bool get isInstallable => false;
}

enum DisplayMode { STANDALONE, BROWSER_TAB }
