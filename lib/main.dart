import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';

import 'screens/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Future getUserInfo() async {
    /*await getUser();
    setState(() {});
    print(uid);*/
  }

  @override
  void initState() {
    getUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DynamicTheme(
      defaultBrightness: Brightness.light,
      data: (brightness) {
        return brightness == Brightness.light
            ? ThemeData(
                primarySwatch: Colors.green,
                backgroundColor: Colors.white,
                cardColor: Colors.green[50],
                primaryTextTheme: TextTheme(
                  button: TextStyle(
                    color: Colors.green,
                    decorationColor: Colors.green[300],
                  ),
                  subtitle2: TextStyle(
                    color: Colors.green[900],
                  ),
                  subtitle1: TextStyle(
                    color: Colors.black,
                  ),
                  headline1: TextStyle(color: Colors.green[800]),
                ),
                bottomAppBarColor: Colors.green[900],
                iconTheme: IconThemeData(color: Colors.green),
                brightness: brightness,
              )
            : ThemeData(
                primarySwatch: Colors.green,
                backgroundColor: Colors.grey[900],
                cardColor: Colors.black,
                primaryTextTheme: TextTheme(
                  button: TextStyle(
                    color: Colors.green[200],
                    decorationColor: Colors.green[50],
                  ),
                  subtitle2: TextStyle(
                    color: Colors.white,
                  ),
                  subtitle1: TextStyle(
                    color: Colors.green[300],
                  ),
                  headline1: TextStyle(
                    color: Colors.white70,
                  ),
                ),
                bottomAppBarColor: Colors.black,
                iconTheme: IconThemeData(color: Colors.green[200]),
                brightness: brightness,
              );
      },
      themedWidgetBuilder: (context, data) => MaterialApp(
        title: 'extinction.email',
        theme: data,
        debugShowCheckedModeBanner: false,
        home: HomePage(),
      ),
    );
  }
}
